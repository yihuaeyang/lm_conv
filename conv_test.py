# Produces "golden" answer for the gtest. Curretly verify by eyeball. :-p

import numpy as np
from scipy import signal

if __name__ == '__main__':
    image = np.reshape([-1.0 + i * 0.125 for i in range(0, 25)], (5, 5))
    print("image = \n%s" % image)

    kernel3 = np.reshape([-80 + i * 16.0 for i in range(0, 9)], (3, 3))
    print("kernel3 = \n%s" % kernel3)
    print("result = \n%s" %
          signal.convolve2d(image, np.rot90(kernel3, 2), 'same'))

    kernel4 = np.reshape([-80 + i * 8.0 for i in range(0, 16)], (4, 4))
    print("kernel4 = \n%s" % kernel4)
    print("result = \n%s" %
          signal.convolve2d(image, np.rot90(kernel4, 2), 'same'))
