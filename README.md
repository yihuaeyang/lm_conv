# README #

### Build and Run Test
To build and run the test, simply do

```
$ make test
```

This should build on any Linux machine with not-too-old gcc supporting C++11.
The only dependence is [GoogleTest](https://github.com/google/googletest),
whose source should be available at the path specified by the environment
variable `$GOOGLETEST`.

The test simply prints inputs and results of few simple convolutions. 

### Check Result
To show the "right" answers corresponding to the C++ test, run

```
$ python3 conv_test.py
```

At present, output from the C++ routines were verified manually. There are
better ways to programmatically check for correctness, but in the interest
of saving time, I'm not investing into it.

### Misc.
* By design, the functions should work for non-square images and kernsl.
No testing is done on that, yet.
* Currently `ConvIm2Cols` first transforms copies of the image into
  k^2 (or ki * kj) **rows** , then transpose them into columns for efficient
  dot-product with the kernel. Alternatively, with SIMD instructions it's
  possible to perform convolution efficiently without that transposition,
  but it would've been called "im2row", not "im2col" convolution. :-)

