#  TARGETS       - list of binary targets (used by make_usage.inc)
#  TESTS         - list of unittest targets (used by make_gtest.inc)
#  ROOT_DIR      - root of the package (automatically added as include dir)
#  INCL_DIRS     - list of include directories
#  LD_LIB_DIRS   - list of library directories (<dir>s for -L<dir>)
#  LD_RPATH_DIRS - list of run-time path dirs (<dir>s for -rpath=<dir>)
#  LD_LIBS       - list of libraries (<lib>s for -l<lib>)
#  EXTRA_FLAGS   - list of additional flags to compiler (e.g. -fopenmp -Dxxx)

MARCH  ?= native
CSTD   ?= -std=gnu11
CXXSTD ?= -std=c++11

AR   ?= ar
CC   ?= gcc
CXX  ?= g++
LD   ?= ld
MAKE ?= make

GOOGLETEST ?= $(HOME)/git-repos/googletest
GTESTDIR = $(GOOGLETEST)/googletest
GMOCKDIR = $(GOOGLETEST)/googlemock
GTESTFLAGS = -isystem $(GTESTDIR)/include -isystem $(GMOCKDIR)/include -I$(GTESTDIR) -I$(GMOCKDIR)

DEBUG_LOG_USE_STDOUT ?= 0
EXTRA_FLAGS += -DDEBUG_LOG_ENABLED -DDEBUG_LOG_USE_STDOUT=$(DEBUG_LOG_USE_STDOUT)

EXTRA_WARNING_FLAGS ?= -Wshadow -Wextra

LDFLAGS = \
  $(foreach dir,$(LD_LIB_DIRS),-L$(dir)) \
  $(foreach dir,$(LD_RPATH_DIRS),-Wl,-rpath=$(dir))

LDLIBS = $(foreach lib,$(LD_LIBS),-l$(lib))

CFLAGS = \
  -march=$(MARCH) -O2 -fPIC -Wall $(EXTRA_WARNING_FLAGS) $(CSTD) \
  -I$(ROOT_DIR) $(foreach dir,$(INCL_DIRS),-I$(dir))

CXXFLAGS = \
  -march=$(MARCH) -O2 -fPIC -Wall $(EXTRA_WARNING_FLAGS) $(CXXSTD) \
  -I$(ROOT_DIR) $(foreach dir,$(INCL_DIRS),-I$(dir))

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	LIB_RT =
	PTHREAD =
else
	LIB_RT = -lrt
	PTHREAD = -pthread
endif

# Below are commands for automatically resolving prerequisites.
#
# C/C++ source (both .c/.cc/.cpp and .h/.hh) directory
SRC_DIR ?= .

DEPDIR = .dep
$(shell mkdir -p $(DEPDIR) > /dev/null)
DEPFLAGS      = -MMD -MF $(DEPDIR)/$*.Td
DEPFLAGS_TEST = -MMD -MF $(DEPDIR)/$*_test.Td
POSTCOMPILE      = mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d
POSTCOMPILE_TEST = mv -f $(DEPDIR)/$*_test.Td $(DEPDIR)/$*_test.d

# Deletes built-in rules for building .o; only use our rules instead.
%.o : $(SRC_DIR)/%.cpp
%.o : $(SRC_DIR)/%.cc
%.o : $(SRC_DIR)/%.c

# %_test.o rules need to be in front of %.o rules to avoid being shadowed.

%_test.o : $(SRC_DIR)/%_test.cc $(DEPDIR)/%_test.d
	$(CXX) $(DEPFLAGS_TEST) -c $< -o $@ $(CXXFLAGS) $(EXTRA_FLAGS) $(GTESTFLAGS)
	$(POSTCOMPILE_TEST)

%_test.o : $(SRC_DIR)/%_test.cpp $(DEPDIR)/%_test.d
	$(CXX) $(DEPFLAGS_TEST) -c $< -o $@ $(CXXFLAGS) $(EXTRA_FLAGS) $(GTESTFLAGS)
	$(POSTCOMPILE_TEST)

%.o : $(SRC_DIR)/%.cc $(DEPDIR)/%.d
	$(CXX) $(DEPFLAGS) -c $< -o $@ $(CXXFLAGS) $(EXTRA_FLAGS)
	$(POSTCOMPILE)

%.o : $(SRC_DIR)/%.cpp $(DEPDIR)/%.d
	$(CXX) $(DEPFLAGS) -c $< -o $@ $(CXXFLAGS) $(EXTRA_FLAGS)
	$(POSTCOMPILE)

%.o : $(SRC_DIR)/%.c $(DEPDIR)/%.d
	$(CC) $(DEPFLAGS) -c $< -o $@ $(CFLAGS) $(EXTRA_FLAGS)
	$(POSTCOMPILE)

# Empty rule causes 'make' NOT to fail if $(DEPDIR)/%.d doesn't exist.
$(DEPDIR)/%.d: ;

# Tells 'make' NOT to delete $(DEPDIR)/%.d as intermediates.
.PRECIOUS: $(DEPDIR)/%.d

# Includes dependency files by translating *.c, *.cc, *.cpp to $(DEPDIR)/%.d.
# Avoids error on non-existent files by using '-include' instead of 'include'.
-include $(patsubst $(SRC_DIR)/%.cpp,$(DEPDIR)/%.d,$(wildcard $(SRC_DIR)/*.cpp))
-include $(patsubst $(SRC_DIR)/%.cc,$(DEPDIR)/%.d,$(wildcard $(SRC_DIR)/*.cc))
-include $(patsubst $(SRC_DIR)/%.c,$(DEPDIR)/%.d,$(wildcard $(SRC_DIR)/*.c))
