#include "lm_conv.hh"

#include "gtest/gtest.h"
#include <cstdio>
#include <memory>
#include <random>
#include <sstream>
#include <string>

namespace lm_conv {

// Custom gtest output
class TestCout : public std::stringstream {
 public:
  ~TestCout() { printf("%s",str().c_str()); }
};
#define COUT TestCout()


using Scalar = float;
using Tensor2D = Tensor<Scalar, 2>;

class ConvDirectTest : public ::testing::Test {
 protected:
  void PrintTensor(const Tensor2D& tensor, const std::string& name) {
    COUT << name <<":\n  "<< tensor.ToString(", ", "\n  ") << std::endl;
  }

  void RunConvs(const Tensor2D& kernel) {
    PrintTensor(kernel, "Kernel");

    Tensor2D result_direct = ConvDirect(*image_, kernel);
    PrintTensor(result_direct, "Result (direct)");

    Tensor2D result_im2col = ConvIm2Col(*image_, kernel);
    PrintTensor(result_im2col, "Result (im2col)");
  }

  void SetUp() override {
    image_ = std::unique_ptr<Tensor2D>(new Tensor2D({5, 5}));
    for (int i = 0; i < (int)image_->Size(); ++i) {
      (*image_)[i] = (i - 8) * 0.125;
    }
    PrintTensor(*image_, "Image");
  }

  void TearDown() override {}

  std::unique_ptr<Tensor2D> image_;
};


TEST_F(ConvDirectTest, kernel1x1)
{
  Tensor2D kernel({1, 1});
  kernel[0] = 2.0;
  RunConvs(kernel);
}


TEST_F(ConvDirectTest, kernel3x3)
{
  Tensor2D kernel({3, 3});
  for (int i = 0; i < (int)kernel.Size(); ++i) {
    kernel[i] = (i - 5) * 16.0;
  }
  RunConvs(kernel);
}


TEST_F(ConvDirectTest, kernel4x4)
{
  Tensor2D kernel({4, 4});
  for (int i = 0; i < (int)kernel.Size(); ++i) {
    kernel[i] = (i - 10) * 8.0;
  }
  RunConvs(kernel);
}

}  // namespace lm_conv
