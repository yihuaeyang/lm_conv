#include "lm_conv.hh"
#include "tensor.hh"

#include <algorithm>
#include <iostream>

namespace lm_conv {

// Performs direct convolution of `image` by `kernel`.
Tensor<float, 2> ConvDirect(const Tensor<float, 2>& image,
                            const Tensor<float, 2>& kernel)
{
  // NOTE: Both image and kernel are row-major, thus `x` and `i` index rows,
  //    while `y` and `j` index columns.
  const int nx = image.Extent(0);   // image has nx rows
  const int ny = image.Extent(1);   // image has ny columns
  const int ki = kernel.Extent(0);  // kernel has ki rows
  const int kj = kernel.Extent(1);  // kernel has kj columns

  Tensor<float, 2> result(image.shape());

  int offset = 0;  // Offset of current result element.
  for (int x = 0; x < nx; ++x) {
    const int i_min = std::max(ki/2 - x, 0);          // inclusive
    const int i_max = std::min(nx - (x - ki/2), ki);  // exclusive

    for (int y = 0; y < ny; ++y) {
      const int j_min = std::max(kj/2 - y, 0);          // inclusive
      const int j_max = std::min(ny - (y - kj/2), kj);  // exclusive

      int kernel_offset = i_min * kj + j_min;
      int image_offset_delta = (i_min - ki/2) * ny + (j_min - kj/2);
      for (int i = i_min; i < i_max; ++i) {
        for (int j = j_min; j < j_max; ++j) {
          result[offset] += (image[offset + image_offset_delta] *
                             kernel[kernel_offset]);
          ++kernel_offset;
          ++image_offset_delta;
        }
        kernel_offset +=  kj - (j_max - j_min);
        image_offset_delta += ny - (j_max - j_min);
      }

      ++offset;
    }
  }
  return result;
}


Tensor<float, 2> Im2Rows(const Tensor<float, 2>& image,
                         const int ki, const int kj)
{
  constexpr int SCALAR_SIZE = sizeof(*image.data());
  const int nx = image.Extent(0);  // image has nx rows
  const int ny = image.Extent(1);  // image has ny columns

  const int img_size = nx * ny;  // no. scalars in image
  const int knl_size = ki * kj;  // no. scalars in kernel
  Tensor<float, 2> im_rows({knl_size, img_size});

#ifdef DEBUG
  for (int k = 0; k < knl_size; ++k) {
    std::cerr <<"row#"<< k <<" = "<< im_rows.data({k, 0}) << std::endl;
  }
#endif

  // Result row# ((ki/2)*kj + kj/2) has the image scalars to multiple the
  // the "middle" kernel scalar, and is a copy of the image itself.
  float* r_row_data = im_rows.mutable_data() + ((ki/2) * kj + kj/2) * img_size;
#ifdef DEBUG
  std::cerr <<"r_row_data = "<< r_row_data << std::endl;
#endif
  memcpy(r_row_data, image.data(), img_size * SCALAR_SIZE);

  for (int x = 0; x < nx; ++x) {  // nx segments, each ny scalars
    // Every segment is right-shifted j scalars and copied j rows above,
    // for j = 1 .. (kj/2).
    for (int j = 1; j <= kj/2; ++j) {
      memcpy(r_row_data + (x * ny) - (j * img_size) + j,
             r_row_data + (x * ny),
             (ny - j) * SCALAR_SIZE);
    }

    // Every segment is left-shifted j scalars and copied j rows below,
    // for j = 1 .. (kj - 1 - kj/2).
    for (int j = 1; j <= (kj - 1 - kj/2); ++j) {
      memcpy(r_row_data + (x * ny) + (j * img_size),
             r_row_data + (x * ny) + j,
             (ny - j) * SCALAR_SIZE);
    }
  }

  r_row_data -= kj/2 * img_size;  // Point to top-most row populated above.
  for (int j = 0; j < kj; ++j) {  // Further copy kj rows populated above.
#ifdef DEBUG
    std::cerr <<"r_row_data = "<< r_row_data << std::endl;
#endif

    // Each row is right-shifted (i*ny) scalars and copied (i*kj) rows above,
    // for i = 1 .. (ki/2).
    for (int i = 1; i <= ki/2; ++i) {
      memcpy(r_row_data + (i * ny) - i * (kj * img_size), r_row_data,
             (img_size - (i * ny)) * SCALAR_SIZE);
    }

    // Each rows is left-shifted (i*ny) scalars and copioed (i*kj) rows below,
    // for i = 1 .. (ki - 1 - ki/2).
    for (int i = 1; i <= (ki - 1 - ki/2); ++i) {
      memcpy(r_row_data + i * (kj * img_size), r_row_data + (i * ny),
             (img_size - (i * ny)) * SCALAR_SIZE);
    }
    r_row_data += img_size;
  }
  return im_rows;
}


// Performs "im2col" convolution of `image` by `kenel`.
Tensor<float, 2> ConvIm2Col(const Tensor<float, 2>& image,
                            const Tensor<float, 2>& kernel)
{
  const int nx = image.Extent(0);   // image has nx rows
  const int ny = image.Extent(1);   // image has ny columns
  const int ki = kernel.Extent(0);  // kernel has ki rows
  const int kj = kernel.Extent(1);  // kernel has kj columns

  Tensor<float, 2> result(image.shape());

  Tensor<float, 2> im_rows = Im2Rows(image, ki, kj);
#ifdef DEBUG
  std::cerr <<"im_rows = \n  "<< im_rows.ToString() << std::endl;
#endif

  Tensor<float, 2> im_cols = im_rows.Transpose({1, 0});
#ifdef DEBUG
  std::cerr <<"im_cols = \n  "<< im_cols.ToString() << std::endl;
#endif

  float* result_data = result.mutable_data();
  const float* im_cols_row = im_cols.data();
  for (int n = 0; n < nx * ny; ++n) {
    for (int k = 0; k < ki * kj; ++k) {
      *result_data += im_cols_row[k] * kernel[k];
    }
    ++result_data;
    im_cols_row += ki * kj;
  }
  return result;
}

}  // namespace lm_conv
