#pragma once

#include "tensor.hh"

namespace lm_conv {

// Performs direct convolution of `image` by `kernel`.
Tensor<float, 2> ConvDirect(const Tensor<float, 2>& image,
                            const Tensor<float, 2>& kernel);

// Returns the "im2row" tensor of `image` for conv with ki-by-kj kernel.
// Given an nx-by-ny image, its "im2row" tensor for conv with ki-by-kj kernel
// has shape (ki*kj)-by-(nx*ky), where --
//  - Each row have the image scalars to multiply one kernel scalar.
//  - Each column have the image scalar to produce one result scalar.
Tensor<float, 2> Im2Rows(const Tensor<float, 2>& image,
                         const int ki, const int kj);

// Performs "im2col" convolution of `image` by `kenel`.
Tensor<float, 2> ConvIm2Col(const Tensor<float, 2>& image,
                            const Tensor<float, 2>& kernel);
}  // namespace lm_conv
