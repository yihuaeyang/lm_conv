#pragma once

#include <err.h>

#include <array>
#include <cstddef>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

namespace lm_conv {

//typedef ptrdiff_t SSIZE_T;
typedef int SSIZE_T;

template <typename Scalar, int RANK>
class Tensor {
 public:
  using Shape = typename std::array<SSIZE_T, RANK>;

  explicit Tensor(const Shape shape)
      : shape_(std::move(shape)),
        buffer_(Volume(-1)) {}

  Tensor(const Tensor& tensor) = default;
  Tensor& operator=(const Tensor& tensor) = default;

  // Returns number of elements in the tensor.
  SSIZE_T Size() const { return (SSIZE_T)buffer_.size(); }

  // Returns the extent (no. elements along the dimension) at given index.
  // NOTE: `index` must be >= 0 and < RANK or dies.
  SSIZE_T Extent(int index) const { return shape_[index]; }

  // Returns no. elements skipped when increasing offset at `index` by 1.
  // NOTE: Set `index` to -1 counts no. elements in tensor using `shape_`.
  SSIZE_T Volume(int index) const;

  // Returns offset of scalar at `indices`.
  SSIZE_T FindOffset(const Shape& indices) const;

  // Returns a new Tensor as transpose of this one.
  // TODO: handles errors in `axis_map`
  Tensor Transpose(const Shape& axis_map) const;

  std::string ToString(const char* val_sep = ", ",
                       const char* row_sep = "\n  ",
                       int precision = 7) const;

  const Scalar& operator[](SSIZE_T offset) const { return buffer_[offset]; }
  const Scalar& operator[](const Shape& indices) const {
    return operator[](FindOffset(indices));
  }

  Scalar& operator[](SSIZE_T offset) { return buffer_[offset]; }
  Scalar& operator[](const Shape& indices) {
    return operator[](FindOffset(indices));
  }

  const Scalar* data(SSIZE_T offset = 0) const {
    return buffer_.data()+offset;
  }
  const Scalar* data(const Shape& indices) const {
    return data(FindOffset(indices));
  }

  Scalar* mutable_data(SSIZE_T offset = 0) { return buffer_.data() + offset; }
  Scalar* mutable_data(const Shape& indices) {
    return mutable_data(FindOffset(indices));
  }

  const Shape& shape() const { return shape_; }

 private:
  // Returns false if reaching the end (outside of tensor).
  static bool NextIndices(const Shape& shape, Shape* indices);

  Shape shape_;
  std::vector<Scalar> buffer_;
};  // class Tensor


template <typename Scalar, int RANK>
SSIZE_T Tensor<Scalar, RANK>::Volume(int index) const
{
  SSIZE_T volume = 1;
  for (int i = RANK - 1; i > index; --i) {
    volume *= Extent(i);
  }
  return volume;
}


template <typename Scalar, int RANK>
SSIZE_T Tensor<Scalar, RANK>::FindOffset(const Shape& indices) const
{
  SSIZE_T offset = indices[0];
  for (int i = 1; i < RANK; ++i) {
    offset = offset * Extent(i) + indices[i];
  }
  return offset;
}


template <typename Scalar, int RANK>
bool Tensor<Scalar, RANK>::NextIndices(const Shape& shape, Shape* indices)
{
  for (int i = RANK - 1; i > 0; --i) {
    if ((*indices)[i] + 1 < shape[i]) {
      (*indices)[i] = (*indices)[i] + 1;
      return true;
    }
    (*indices)[i] = 0;
  }
  ++(*indices)[0];
  return (*indices)[0] < shape[0];
}


template <typename Scalar, int RANK>
Tensor<Scalar,RANK> Tensor<Scalar,RANK>::Transpose(const Shape& axis_map) const
{
  Shape ret_shape;
  for (int axis = 0; axis < (int)ret_shape.size(); ++axis) {
    int src_axis = axis_map[axis];
    ret_shape[axis] = shape_[src_axis];
  }
  Tensor<Scalar, RANK> ret(ret_shape);

  Shape cur_pos = {0};
  Shape ret_pos = {0};
  ret[0] = buffer_[0];
  for (SSIZE_T i = 1; i < Size(); ++i) {
    NextIndices(ret_shape, &ret_pos);
    for (int ret_i = 0; ret_i < RANK; ++ret_i) {
      int cur_i = axis_map[ret_i];
      cur_pos[cur_i] = ret_pos[ret_i];
    }
    ret[i] = operator[](cur_pos);
  }
  return ret;
}


template <typename Scalar, int RANK>
std::string Tensor<Scalar, RANK>::ToString(const char* val_sep,
                                           const char* row_sep,
                                           int precision) const
{
  std::stringstream ss;
  ss.precision(precision);

  int col_n = 0;
  for (auto it = buffer_.cbegin(); it != buffer_.cend(); ++it) {
    ss << std::setw(precision) << *it;
    if (++col_n < Extent(RANK-1)) {
      ss << val_sep;
    } else if (&*it != &buffer_.back()) {
      ss << row_sep;
      col_n = 0;
    }
  }
  return ss.str();
}


template <typename S, int R>
bool operator==(const Tensor<S, R>& arg1, const Tensor<S, R>& arg2) {
  return arg1.shape() == arg2.shape() && arg1.buffer() == arg2.buffer();
}

template <typename S, int R>
bool operator!=(const Tensor<S, R>& arg1, const Tensor<S, R>& arg2) {
  return !(arg1 == arg2);
}

}  // namespace lm_conv
