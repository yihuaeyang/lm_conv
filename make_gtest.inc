# Only to be included after `make_common.inc`.

TESTS += \
  $(patsubst %_test.cc,%.test,$(wildcard *_test.cc))  \
  $(patsubst %_test.cpp,%.test,$(wildcard *_test.cpp))

# Makes sure tests are always run -- even if already built.
.PHONY: tests *.test

# Required to have this rule again after TESTS is updated above.
tests: $(TESTS)

gtest_main.a: $(ROOT_DIR)/gtest-all.o $(ROOT_DIR)/gtest_main.o
	$(AR) rv $@ $^

gmock_main.a: $(ROOT_DIR)/gtest-all.o $(ROOT_DIR)/gmock-all.o $(ROOT_DIR)/gmock_main.o
	$(AR) rv $@ $^

$(ROOT_DIR)/gtest-all.o:
	$(CXX) -o $@ -c $(GTESTDIR)/src/gtest-all.cc $(GTESTFLAGS) $(CXXFLAGS)

$(ROOT_DIR)/gtest_main.o:
	$(CXX) -o $@ -c $(GTESTDIR)/src/gtest_main.cc $(GTESTFLAGS) $(CXXFLAGS)

$(ROOT_DIR)/gmock-all.o:
	$(CXX) -o $@ -c $(GMOCKDIR)/src/gmock-all.cc $(GTESTFLAGS) $(CXXFLAGS)

$(ROOT_DIR)/gmock_main.o:
	$(CXX) -o $@ -c $(GMOCKDIR)/src/gmock_main.cc $(GTESTFLAGS) $(CXXFLAGS)

.PHONY: gtest_clean
gtest_clean:
	rm -f $(ROOT_DIR)/*.o gtest_main.a gmock_main.a
