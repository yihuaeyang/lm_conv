
.PHONY: build
build:
	$(MAKE) -C src lm_conv.a

.PHONY: test
test:
	$(MAKE) -C src tests

.PHONY: clean
clean:
	$(MAKE) -C src clean
